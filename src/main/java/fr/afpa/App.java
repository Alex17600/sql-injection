package fr.afpa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.*;

/**
 * Application permettant d'illustrer les failles liées aux injection SQL.
 */
public final class App {
    private static Connection connection;

    public static void main(String[] args) {
        establishConnection();

        System.out.println("Programme de test d'accès à une base de données");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Login (adresse e-mail) : ");
        String email = scanner.nextLine();
        
        if (checkEmailRegex(email) == false) {
            System.out.println("Error : " + email + " is not a valid mail");
            scanner.close();
            return;
        } else System.out.println("-> e-mail utilisateur saisi : " + email);


        System.out.println("Saisissez votre mot de passe pour obtenir des détails sur votre compte :");
        String password = scanner.nextLine();
        System.out.println("-> Mot de passe saisi : " + password);
        
        printUserInformationByFunctionCall(email, password);

        System.out.println("Fin du programme");
        // fermeture du scanner pour éviter les fuites mémoire
        scanner.close();

        // Ne pas oublier de fermer la connexion à la base de données en fin de programme
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void establishConnection() {
        try {
            String password = "Djaltek30";
            App.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/injection_test", "postgres",
                    password);
        } catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données.");
            System.err.println(e.getMessage());
        }
    }

    public static boolean checkEmailRegex(String mail) {
        String regexEmail = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        return Pattern.matches(regexEmail, mail);
    }

    /**
     * Affiche les informations d'un utilisateur retrouvé en BDD via son email et
     * son mot de passe.
     */
    // private static void printUserInformation(String email, String password) {
    //     try {
    //         PreparedStatement statement = connection.prepareStatement("select firstname, lastname, country, password from \"user\" where email = ? and password = ?");
    //         System.out.println("-> (Debug) Requête qui sera exécutée : " + statement);
    //         statement.setString(1, email);
    //         statement.setString(2, password);
    //         ResultSet resultSet = statement.executeQuery();

    //         int count = 0;
    //         if (resultSet != null) {
    //             while (resultSet.next()) {
    //                 System.out.println("Nom : " + resultSet.getString("firstname"));
    //                 System.out.println("Prénom : " + resultSet.getString("lastname"));
    //                 System.out.println("Pays : " + resultSet.getString("country"));
    //                 System.out.println("Mot de passe : " + resultSet.getString("password"));
    //                 count++;
    //             }
    //             resultSet.close();
    //             statement.close();
    //         }

    //         if (count == 0) {
    //             System.out.println("Aucune résultat retrouvé. Vérifiez votre identifiant ou votre mot de passe.");
    //         }

    //     } catch (SQLException e) {
    //         System.err.println(e.getMessage());
    //     }
    // }

    public static void printUserInformationByFunctionCall(String email, String password) {
        try {

            PreparedStatement ps = connection.prepareStatement("select * from find_user_info(?, ?)");
            ps.setString(1, email);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                    System.out.println("Nom : " + rs.getString("firstname"));
                    System.out.println("Prénom : " + rs.getString("lastname"));
                    System.out.println("Pays : " + rs.getString("country"));
                    System.out.println("Mot de passe : " + rs.getString("password"));
            }

        } catch (SQLException sqlException) {
            System.err.println(sqlException.getMessage());
        }
    }
}
